try:
    from trytond.modules.tributi.tests.test_tributi import suite
except ImportError:
    from .test_tributi import suite

__all__ = ['suite']
