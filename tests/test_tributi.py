import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class TributiTestCase(ModuleTestCase):
    'Test Tributi module'
    module = 'tributi'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            TributiTestCase))
    return suite
