from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool

__all__ = ['Bolletta']



class Bolletta(ModelSQL, ModelView):
    'Bolletta'
    __name__ = 'tributi.bolletta'
    name = fields.Char('Name')
    code = fields.Char('Code')
    tipo = fields.Selection([
        ('commerciale', 'Commerciale'),
        ('mortuaria', 'Mortuaria'),
        ('sociale', 'Sociale'),
        ('istituzionale', 'Istituzionale')
    ], 'tipo')
    
    party = fields.Many2One('party.party', 'Party', required=True, select=True,)

    dataCommissione = fields.Date('Data Commissione')
    dataInizio = fields.Date('Data Inizio', required=True)
    dataFine = fields.Date('Data Fine', required=True)
    
    numeroGiorni = fields.Function(fields.Integer('Numero Giorni'),'on_change_with_numeroGiorni')            
    @fields.depends('dataInizio', 'dataFine')
    def on_change_with_numeroGiorni(self, name=None):
        if self.dataInizio and self.dataFine:
            return abs((self.dataFine - self.dataInizio).days) + 1
        return 0

    quantita = fields.Integer('Quantità', required=True)

    formato = fields.Selection([
        ('1', '1'),
        ('2', '2'),
        ('4', '4'),
        ('8', '8'),
        ('12','12'),
        ('16', '16'),
        ('24', '24')
    ], 'formato', required=True)

    
    numeroFogli = fields.Function(fields.Integer('Numero Fogli'), 'on_change_with_numeroFogli')

    @fields.depends('formato', 'quantita')
    def on_change_with_numeroFogli(self, name=None):
        if self.formato and self.quantita:
            return int(self.formato) * self.quantita
        return 0

    M50 = fields.Function(fields.Boolean('M50'), 'on_change_with_M50')
    
    @fields.depends('numeroFogli')
    def on_change_with_M50(self, name=None):
        if self.numeroFogli >= 50:
            return True
        else:
            return False
        
    M812 = fields.Function(fields.Boolean('M812'), 'on_change_with_M812')
        
    @fields.depends('formato')
    def on_change_with_M812(self, name=None):
        if int(self.formato) > 7 and int(self.formato) < 13:
            return True
        else:
            return False

    fattoreGiorni = fields.Function(fields.Numeric('fattore giorni'), 'on_change_with_fattoreGiorni')

    @fields.depends('numeroGiorni')
    def on_change_with_fattoreGiorni(self, name=None):
        return (self.numeroGiorni - 10) / 5

    M16 = fields.Function(fields.Boolean('M16'), 'on_change_with_M16')

    @fields.depends('formato')
    def on_change_with_M16(self, name=None):
        if int(self.formato) > 15:
            return True
        else:
            return False

    prezzoUnitario = fields.Function(fields.Numeric('prezzoUnitario'), 'on_change_with_prezzoUnitario')

    @fields.depends('formato')
    def on_change_with_prezzoUnitario(self, name=None):
        if int(self.formato) == 1:
            return 1.6112
        else:
            return 2.014


    maggiorazioneNumeroGiorni = fields.Function(fields.Numeric('maggiorazioneNumeroGiorni'), 'on_change_with_maggiorazioneNumeroGiorni')

    @fields.depends('formato')
    def on_change_with_maggiorazioneNumeroGiorni(self, name=None):
        if int(self.formato) == 1:
            return 0.4833
        else:
            return 0.6042


    tariffaBase = fields.Function(fields.Numeric('tariffaBase'), 'on_change_with_tariffaBase')

    @fields.depends('prezzoUnitario', 'fattoreGiorni', 'maggiorazioneNumeroGiorni')
    def on_change_with_tariffaBase(self, name=None):
        return self.prezzoUnitario + self.fattoreGiorni * self.maggiorazioneNumeroGiorni


    subtotale = fields.Function(fields.Numeric('subtotale'),'on_change_with_subtotale')

    @fields.depends('tariffaBase', 'quantita')
    def on_change_with_subtotale(self, name=None):
        return self.tariffaBase * self.quantita

    #SubTotale = TariffaBase * QuantitàManifesti

    #PrezzoUnitario + FattoreGiorni * MaggiorazioneNumeroGiorni
